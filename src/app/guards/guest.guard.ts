import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GuestGuard implements CanActivate {
  constructor(private router: Router) { }

  canActivate(): boolean {
    const guard: any = localStorage.getItem('payload') ? JSON.parse(localStorage.getItem('payload') || "") : null
    const validateAccess: boolean = (guard?.token != undefined || guard?.token != null) ? true : false

    if (validateAccess) {
      this.router.navigateByUrl('home')
      return !validateAccess
    }

    localStorage.clear()
    return !validateAccess
  }
}
