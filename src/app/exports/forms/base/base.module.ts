import { NgModule } from '@angular/core';
import { TableModule } from 'primeng/table';
import { PanelModule } from 'primeng/panel';
import { ToastModule } from 'primeng/toast';
import { DialogModule } from 'primeng/dialog';
import { AvatarModule } from 'primeng/avatar';
import { ButtonModule } from 'primeng/button';
import { ToolbarModule } from 'primeng/toolbar';
import { SidebarModule } from 'primeng/sidebar';
import { TooltipModule } from 'primeng/tooltip';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { PickListModule } from 'primeng/picklist';
import { PaginatorModule } from 'primeng/paginator';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';
import { KeyFilterModule } from 'primeng/keyfilter';
import { MatSortModule } from '@angular/material/sort';
import { MatMenuModule } from '@angular/material/menu'
import { MatIconModule } from '@angular/material/icon';
import { InputNumberModule } from 'primeng/inputnumber';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
  ],
  imports: [
    PanelModule,
    FormsModule,
    ToastModule,
    TableModule,
    AvatarModule,
    DialogModule,
    ButtonModule,
    MatSortModule,
    ToolbarModule,
    TooltipModule,
    MatIconModule,
    MatMenuModule,
    SidebarModule,
    CalendarModule,
    DropdownModule,
    CheckboxModule,
    MatTableModule,
    PickListModule,
    MatButtonModule,
    PaginatorModule,
    InputMaskModule,
    InputTextModule,
    KeyFilterModule,
    MatTooltipModule,
    InputNumberModule,
    MatPaginatorModule,
    InputTextareaModule,
    ReactiveFormsModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
  ],
  exports: [
    FormsModule,
    ToastModule,
    TableModule,
    AvatarModule,
    DialogModule,
    ButtonModule,
    MatSortModule,
    ToolbarModule,
    TooltipModule,
    MatIconModule,
    MatMenuModule,
    SidebarModule,
    CalendarModule,
    PickListModule,
    DropdownModule,
    CheckboxModule,
    MatTableModule,
    MatButtonModule,
    PaginatorModule,
    InputMaskModule,
    InputTextModule,
    KeyFilterModule,
    MatTooltipModule,
    InputNumberModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    InputTextareaModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
  ]
})
export class BaseModule { }
