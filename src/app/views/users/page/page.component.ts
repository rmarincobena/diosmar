import Swal from 'sweetalert2';
import * as moment from 'moment';
import { User } from '../model/user';
import { IUser } from '../interface/iuser';
import { Titles } from 'src/app/enums/titles';
import { Component, OnInit } from '@angular/core';
import { Severities } from 'src/app/enums/severities';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UsersService } from 'src/app/services/http/users.service';
import { CommonService } from 'src/app/services/base/common.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

  /* formGroup */
  public userForm!: FormGroup

  /* Table information */
  displayedColumns: string[] = ['identificacion', 'nombres', 'apellidos', 'fechaNac', 'genero', 'activo', 'action'];

  /* Declarations */
  /* Users */
  users: IUser[] = []
  modalUser: boolean = false

  /* Boolean */
  isLoadingSave: boolean = false
  isComplete: boolean = false
  isEdit: boolean = false

  /* Descriptive */
  titleBase: string = "REGISTRAR USUARIO"
  genders: Array<any> = [{ name: "Masculino", value: "M" }, { name: "Femenino", value: "F" }];

  constructor(private fb: FormBuilder, public userHttpSrv: UsersService, public commonSrv: CommonService) {
  }

  ngOnInit(): void {
    /* set forms */
    this.userForm = new User().formBuilder(this.fb)

    /* Calls */
    this.getUsers()
  }

  /* Events */
  /* onClick */
  open() {
    /* set forms */
    this.userForm.reset()
    this.userForm = new User().formBuilder(this.fb)

    this.isEdit = false
    this.titleBase = "REGISTRAR USUARIO"
    this.modalUser = !this.modalUser
  }

  edit(user: User) {
    this.userForm.reset()
    this.userForm = new User().formBuilder(this.fb)
    this.userForm.setValue(new User().newObject(user))

    this.isEdit = true
    this.titleBase = "MODIFICAR USUARIO"
    this.modalUser = !this.modalUser
  }

  destroy(user: User) {
    Swal.fire({
      icon: "warning",
      title: `Confirmación`,
      text: `¿Seguro desea eliminar el registro ${user.identificacion}?`,
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonText: "SI",
      confirmButtonColor: "#3B82F6",
      cancelButtonText: "NO",
    }).then((result) => {
      if (result.isConfirmed)
        this.userDestroy(user.identificacion)
    })
  }

  /* Call APIs */
  getUsers(): void {
    this.users = []
    this.isComplete = !this.isComplete
    this.userHttpSrv.index().subscribe({
      next: (response) => {
        this.isComplete = !this.isComplete
        this.users = response.data
      },
      error: (err) => {
        this.isComplete = !this.isComplete
        this.commonSrv.showToast(Severities.ERROR, Titles.ERROR, `Error del lado del servidor`)
      },
    })
  }

  userStore() {
    this.isLoadingSave = !this.isLoadingSave
    if (!this.isEdit) {
      /* Section Insert */
      /* Validar si la C.I existe */
      let validate = this.users.find(el => el.identificacion === this.userForm.value.identificacion)

      if (validate !== undefined) {
        this.commonSrv.showToast(Severities.WARN, Titles.WARN, `La C.I ya se encuentra registrada!`)
        this.isLoadingSave = !this.isLoadingSave
        return
      }

      this.userForm.value.fechaNac = moment(this.userForm.value.fechaNac).format("YYYY-MM-DD")
      this.users.push(this.userForm.value)

    } else {
      /* Section Edit */
      this.userForm.value.fechaNac = moment(this.userForm.value.fechaNac).format("YYYY-MM-DD")
      this.users[this.findIndexById(this.userForm.value.identificacion)] = this.userForm.value
    }

    this.isLoadingSave = !this.isLoadingSave
    this.modalUser = !this.modalUser
    this.users = [...this.users]
    this.commonSrv.showToast(Severities.SUCCESS, Titles.SUCCESS, `El proceso se ejecuto con exito!`)
  }

  userDestroy(identificacion: string) {
    this.users = this.users.filter(el => el.identificacion !== identificacion)
    Swal.fire(
      '¡Satisfactorio!',
      `Registro eliminado correctamente`,
      Severities.SUCCESS
    )
  }

  findIndexById(id: string): number {
    let index = -1;
    for (let i = 0; i < this.users.length; i++) {
      if (this.users[i].identificacion === id) {
        index = i;
        break;
      }
    }

    return index;
  }
}
