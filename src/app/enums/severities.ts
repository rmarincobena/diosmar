export enum Severities {
    SUCCESS = "success",
    INFO = "info",
    WARN = "warn",
    ERROR = "error",
}
