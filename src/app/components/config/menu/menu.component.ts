import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/base/common.service';
import { LayoutService } from 'src/app/services/layout/layout.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  model: any[] = [];
  usrImage: string = ""

  constructor(public layoutSrv: LayoutService, public commonSrv: CommonService) { }

  ngOnInit(): void {
    this.buildTreeMenu()
  }

  /* Events */
  private buildTreeMenu() {
    let tmpMenu = [
      {
        label: "Home",
        icon: "",
        items: [
          {
            label: "Home",
            icon: "fa-solid fa-house",
            routerLink: ["home"],
          },
        ],
      },
      {
        label: "Configuración",
        icon: "",
        items: [
          {
            label: "Usuarios",
            icon: "fa-solid fa-users",
            routerLink: ["users"],
          },
        ],
      },
      {
        label: "Niveles",
        icon: "",
        items: [
          {
            label: "Level I",
            icon: "fa-solid fa-list",
            items: [
              {
                label: "Level II",
                icon: "fa-solid fa-book",
                items: [
                  {
                    label: "Level III",
                    icon: "fa-solid fa-barcode",
                    routerLink: ["not-found"],
                  },
                ],
              },
            ],
          }
        ],
      },
    ];

    this.model = tmpMenu;
  }
}
