import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/base/common.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

  tenantImage: string = ""

  constructor(public commonSrv: CommonService) { }

  ngOnInit(): void {
    this.tenantImage = `assets/layout/images/landlord/diosmar.png`
  }

}
