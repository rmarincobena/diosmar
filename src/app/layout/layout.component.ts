import { Titles } from '../enums/titles';
import { MessageService } from 'primeng/api';
import { Subscription, filter } from 'rxjs';
import { Severities } from '../enums/severities';
import { Router, NavigationEnd } from '@angular/router';
import { LayoutService } from '../services/layout/layout.service';
import { Component, OnInit, OnDestroy, Renderer2, ViewChild } from '@angular/core';
import { TopBarComponent } from '../components/config/top-bar/top-bar.component';
import { SidebarComponent } from '../components/config/sidebar/sidebar.component';
import { CommonService } from '../services/base/common.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, OnDestroy {

  /* Childs */
  @ViewChild(TopBarComponent) topbarComponent!: any /* TopBarComponent */
  @ViewChild(SidebarComponent) sidebarComponent!: any /* SidebarComponent */

  /* Subscriptions */
  overlayMenuOpenSubscription: Subscription;

  /* Declarations */
  menuOutsideClickListener: any;
  profileMenuOutsideClickListener: any;

  constructor(public layoutSrv: LayoutService, public renderer: Renderer2, public router: Router, public commonSrv: CommonService, public messageSrv: MessageService) {
    this.overlayMenuOpenSubscription = this.layoutSrv.overlayOpen$.subscribe(() => {
      if (!this.profileMenuOutsideClickListener) {
        this.profileMenuOutsideClickListener = this.renderer.listen('document', 'click', event => {
          const isOutsideClicked = !(this.topbarComponent.menu.nativeElement.isSameNode(event.target) || this.topbarComponent.menu.nativeElement.contains(event.target)
            || this.topbarComponent.topbarMenuButton.nativeElement.isSameNode(event.target) || this.topbarComponent.topbarMenuButton.nativeElement.contains(event.target));

          if (isOutsideClicked) {
            this.hideProfileMenu();
          }
        });
      }

      if (this.layoutSrv.state.staticMenuMobileActive) {
        this.blockBodyScroll();
      }
    });

    this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => {
      this.hideMenu();
      this.hideProfileMenu();
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if (this.overlayMenuOpenSubscription) {
      this.overlayMenuOpenSubscription.unsubscribe();
    }

    if (this.menuOutsideClickListener) {
      this.menuOutsideClickListener();
    }
  }

  /* Getter/Setter */
  get containerClass() {
    return {
      'layout-theme-light': this.layoutSrv.config.colorScheme === 'light',
      'layout-theme-dark': this.layoutSrv.config.colorScheme === 'dark',
      'layout-overlay': this.layoutSrv.config.menuMode === 'overlay',
      'layout-static': this.layoutSrv.config.menuMode === 'static',
      'layout-slim': this.layoutSrv.config.menuMode === 'slim',
      'layout-horizontal': this.layoutSrv.config.menuMode === 'horizontal',
      'layout-static-inactive': this.layoutSrv.state.staticMenuDesktopInactive && this.layoutSrv.config.menuMode === 'static',
      'layout-overlay-active': this.layoutSrv.state.overlayMenuActive,
      'layout-mobile-active': this.layoutSrv.state.staticMenuMobileActive,
      'p-input-filled': this.layoutSrv.config.inputStyle === 'filled',
      'p-ripple-disabled': !this.layoutSrv.config.ripple
    }
  }

  /* Events */
  hideMenu(): void {
    this.layoutSrv.state.overlayMenuActive = false;
    this.layoutSrv.state.staticMenuMobileActive = false;
    this.layoutSrv.state.menuHoverActive = false;
    if (this.menuOutsideClickListener) {
      this.menuOutsideClickListener();
      this.menuOutsideClickListener = null;
    }
    this.unblockBodyScroll();
  }

  hideProfileMenu(): void {
    this.layoutSrv.state.profileSidebarVisible = false;
    if (this.profileMenuOutsideClickListener) {
      this.profileMenuOutsideClickListener();
      this.profileMenuOutsideClickListener = null;
    }
  }

  blockBodyScroll(): void {
    if (document.body.classList) {
      document.body.classList.add('blocked-scroll');
    }
    else {
      document.body.className += ' blocked-scroll';
    }
  }

  unblockBodyScroll(): void {
    if (document.body.classList) {
      document.body.classList.remove('blocked-scroll');
    }
    else {
      document.body.className = document.body.className.replace(new RegExp('(^|\\b)' +
        'blocked-scroll'.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }
  }
}
