import { Component, ElementRef, OnInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { ES } from './helpers/es.lang';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private _elementRef: ElementRef, private config: PrimeNGConfig) { }

  ngOnInit() {
    /* set lang */
    this.config.setTranslation(ES);
    this._elementRef.nativeElement.removeAttribute("ng-version")
  }
}
