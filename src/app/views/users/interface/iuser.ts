export interface IUser {
    identificacion: string;
    nombres: string;
    apellidos: string;
    fechaNac: Date;
    genero: string;
    direccion: string;
    telefono: string;
    numeroHijos: number;
    activo?: boolean;
}
