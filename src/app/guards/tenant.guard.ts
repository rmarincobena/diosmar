import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TenantGuard implements CanActivate {
  constructor(private router: Router) { }

  canActivate(): boolean {
    const guard: any = localStorage.getItem('payload') ? JSON.parse(localStorage.getItem('payload') || "") : null
    const validateAccess: boolean = (guard?.token != undefined || guard?.token != null) ? true : false

    if (validateAccess)
      return validateAccess

    localStorage.clear()
    this.router.navigateByUrl('sign-in')

    return validateAccess
  }
}
