import { IPaginator } from "../interfaces/ipaginator";

export class Paginator implements IPaginator {
    /* data implementation */
    per_page: number;
    current_page: number;
    last_page: number;
    from: number;
    to: number;
    total: number;
    sortBy?: string;
    sortDir?: string;

    constructor(sortBy: string, sortDir: string = "desc") {
        this.per_page = 5
        this.current_page = 0
        this.last_page = 0
        this.from = 0
        this.to = 0
        this.total = 0
        this.sortBy = sortBy
        this.sortDir = sortDir
    }
}
