export enum Titles {
    SUCCESS = "Satisfactorio",
    INFO = "Informativo",
    WARN = "Advertencia",
    ERROR = "Error",
}
