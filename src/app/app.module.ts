import { NgModule } from '@angular/core';
import { ToastModule } from 'primeng/toast';
import { MessageService } from "primeng/api";
import { DialogModule } from 'primeng/dialog';
import { AppComponent } from './app.component';
import { GuestGuard } from './guards/guest.guard';
import { TenantGuard } from './guards/tenant.guard';
import { LayoutModule } from './layout/layout.module';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { LayoutComponent } from './layout/layout.component';
import { SignInModule } from './views/sign-in/sign-in.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotFoundModule } from './views/not-found/not-found.module';
import { MenuComponent } from './components/config/menu/menu.component';
import { FooterComponent } from './components/config/footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TopBarComponent } from './components/config/top-bar/top-bar.component';
import { SidebarComponent } from './components/config/sidebar/sidebar.component';
import { MenuItemComponent } from './components/config/menuitem/menuitem.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    TopBarComponent,
    SidebarComponent,
    MenuComponent,
    MenuItemComponent,
    FooterComponent,
  ],
  imports: [
    FormsModule,
    ToastModule,
    DialogModule,
    LayoutModule,
    SignInModule,
    BrowserModule,
    NotFoundModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [
    GuestGuard,
    TenantGuard,
    MessageService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
