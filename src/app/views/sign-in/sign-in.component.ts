import * as uuid from 'uuid';
import { Router } from '@angular/router';
import { SignIn } from './model/sign-in';
import { Titles } from 'src/app/enums/titles';
import { Component, OnInit } from '@angular/core';
import { Severities } from 'src/app/enums/severities';
import { GuestService } from 'src/app/services/http/guest.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from 'src/app/services/base/common.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  /* formGroup */
  public signInForm!: FormGroup

  /* Declarations */
  isLoadingSignIn: boolean = false

  constructor(private fb: FormBuilder, private guestHttpSrv: GuestService, private commonSrv: CommonService, private router: Router) {
  }

  ngOnInit(): void {
    this.signInForm = new SignIn().formBuilder(this.fb)
  }

  /* Call APIs */
  signIn(): void {
    this.isLoadingSignIn = !this.isLoadingSignIn
    this.guestHttpSrv.signIn(this.signInForm.value).subscribe({
      next: (response) => {
        let access = response.data.find((el: any) => el.username === this.signInForm.value.username && el.password === this.signInForm.value.password)

        if (access === undefined) {
          this.commonSrv.showToast(Severities.WARN, Titles.WARN, `El usuario o contraseña son incorrectas`)
          setTimeout(() => {
            this.isLoadingSignIn = !this.isLoadingSignIn
          }, 1500);
          return
        }

        const { username, organization, role } = access

        setTimeout(() => {
          this.isLoadingSignIn = !this.isLoadingSignIn

          localStorage.setItem("payload", JSON.stringify({
            username,
            organization,
            role,
            token: uuid.v4()
          }))

          this.router.navigateByUrl('home')
        }, 1500);
      },
      error: (err) => {
        this.isLoadingSignIn = !this.isLoadingSignIn
        this.commonSrv.showToast(Severities.ERROR, Titles.ERROR, `Error del lado del servidor`)
      }
    })
  }
}
