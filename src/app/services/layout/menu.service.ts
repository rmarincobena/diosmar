import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { IMenuEvent } from '../../config/interfaces/imenu-event';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor() { }

  private menuSource = new Subject<IMenuEvent>();
  private resetSource = new Subject();

  menuSource$ = this.menuSource.asObservable();
  resetSource$ = this.resetSource.asObservable();

  onMenuStateChange(event: IMenuEvent) {
    this.menuSource.next(event);
  }

  reset() {
    this.resetSource.next(true);
  }
}
