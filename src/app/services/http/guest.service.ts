import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../base/common.service';

@Injectable({
  providedIn: 'root'
})
export class GuestService {

  constructor(private http: HttpClient, private commonSrv: CommonService) { }

  signIn(payload: any): Observable<any> {
    return this.http.get<any>(`assets/json/access.json`);
  }
}
