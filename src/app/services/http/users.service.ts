import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../base/common.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient, private commonSrv: CommonService) { }

  index(): Observable<any> {
    return this.http.get<any>(`assets/json/users.json`);
  }
}
