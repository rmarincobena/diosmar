import { ISignIn } from "../interface/isign-in";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

export class SignIn implements ISignIn {
    username: String;
    password: String;

    constructor() {
        this.username = "";
        this.password = "";
    }

    newObject(payload: any): void {
        this.username = payload["username"]
        this.password = payload["password"]
    }

    formBuilder(fb: FormBuilder): FormGroup {
        return fb.group({
            username: [this.username, Validators.compose([Validators.required])!],
            password: [this.password, Validators.compose([Validators.required])!],
        })
    }
}
