import Swal from "sweetalert2";
import { Router } from "@angular/router";
import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Titles } from "../../enums/titles";
import { Messages } from "../../enums/messages";
import { HttpHeaders } from "@angular/common/http";
import { Severities } from "../../enums/severities";
import { Paginator } from "../../config/models/paginator";
import { environment } from 'src/environments/environment';
import { IPaginator } from "../../config/interfaces/ipaginator";

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  /* Declarations */
  paginator: IPaginator = new Paginator("id")
  rowsPerPageOpt: Array<number> = [5, 10, 25, 50]

  constructor(private messageSrv: MessageService, private router: Router) { }

  /* Events */
  /* Http/Storage */
  getBaseUrl(): string {
    return `${environment.apiUrl}/api`
  }

  getPayload() {
    return JSON.parse(localStorage.getItem("payload") || "")
  }

  getHeaderTenant(contentType: string = "application/json"): HttpHeaders {
    return new HttpHeaders({
      Authorization: "Bearer " + JSON.parse(localStorage.getItem("payload") || "")?.token,
    });
  }

  getHeaderGuest(contentType: string = "application/json"): HttpHeaders {
    return new HttpHeaders({
      "Content-Type": contentType,
    });
  }

  /* Toast */
  showToast(severity: string, title: string, message: string, timelife: number = 5000): any {
    this.messageSrv.add({
      severity: severity,
      summary: title,
      detail: message,
      life: timelife,
    });
  }

  /* Sweet Alert */
  sessionExpired(message?: string) {
    Swal.fire({
      icon: 'error',
      title: '¡Ops!',
      text: (message != null) ? message : Messages.UNAUTHORIZED,
      allowOutsideClick: false,
      confirmButtonText: 'OK',
      confirmButtonColor: "#3B82F6",
    }).then((result) => {
      if (result.isConfirmed) {
        this.messageSrv.add({
          severity: Severities.ERROR,
          summary: Titles.ERROR,
          detail: (message != null) ? message : Messages.UNAUTHORIZED,
          life: 5000,
        });
        setTimeout(() => {
          localStorage.clear()
          this.router.navigateByUrl('sign-in')
        }, 1000)
      }
    })
  }

  /* Paginator */
  initPaginator(payload: any) {
    this.paginator = payload;
  }

  paginationPerPage(paginator: any): any {
    paginator.per_page = this.rowsPerPageOpt.find((el) => el >= paginator.per_page)
    return paginator
  }

  eventPaginator(payload: any) {
    let newPaginator = { ...this.paginator, ...{ current_page: payload.page, per_page: payload.rows } }
    this.paginator = newPaginator
  }

  sortPaginator(payload: any) {
    let sortDir = (payload.order === 1) ? "asc" : "desc"
    if (sortDir != this.paginator.sortDir || payload.field != this.paginator.sortBy) {
      let newPaginator = { ...this.paginator, ...{ sortBy: payload.field, sortDir: sortDir } }
      this.paginator = newPaginator
    }
  }

  /* Validators */
  restrictNumeric(evt: any) {
    let key = (evt.which) ? evt.which : evt.keyCode
    return ((key >= 48 && key <= 57) || key == 8 || key == 32)
  }

  restrictAlpha(evt: any) {
    let key = (evt.which) ? evt.which : evt.keyCode
    return ((key >= 65 && key <= 90) || (key >= 97 && key <= 122) || (key >= 164 && key <= 165) || key == 8 || key == 32)
  }

  restrictAlphaNumeric(evt: any) {
    let key = (evt.which) ? evt.which : evt.keyCode
    return ((key >= 48 && key <= 57) || (key >= 65 && key <= 90) || (key >= 97 && key <= 122) || (key >= 164 && key <= 165) || key == 8 || key == 32)
  }
}
