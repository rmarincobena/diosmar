import { Component, OnInit } from '@angular/core';

@Component({
  template: `
  <div class="layout-footer text-center">
    <small class="font-medium ml-2">Políticas de privacidad <span style="color: #f59e0b;">&copy;Copyright {{currentYear}} Grupo DIOSMAR</span> Todos los derechos reservados</small>
  </div>
  `,
  selector: 'app-footer',
})
export class FooterComponent implements OnInit {

  /* Declarations */
  currentYear: any = new Date().getFullYear()

  constructor() { }

  ngOnInit(): void {
  }

}
