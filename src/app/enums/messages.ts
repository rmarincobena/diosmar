export enum Messages {
    SUCCESS = "Proceso ejecutado con exito",
    ERROR = "¡Algo salió mal!",
    UNAUTHORIZED = "Sesión Finalizada"
}
