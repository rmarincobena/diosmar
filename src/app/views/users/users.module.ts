import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageComponent } from './page/page.component';
import { UsersRoutingModule } from './users-routing.module';
import { BaseModule } from 'src/app/exports/forms/base/base.module';

@NgModule({
  declarations: [
    PageComponent
  ],
  imports: [
    BaseModule,
    CommonModule,
    UsersRoutingModule
  ]
})
export class UsersModule { }
