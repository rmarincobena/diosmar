import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/http/users.service';
import { CommonService } from 'src/app/services/base/common.service';
import { LayoutService } from 'src/app/services/layout/layout.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {
  /* Declarations */
  @ViewChild('menubutton') menuButton!: ElementRef;
  @ViewChild('topbarmenubutton') topbarMenuButton!: ElementRef;
  @ViewChild('topbarmenu') menu!: ElementRef;

  constructor(public layoutSrv: LayoutService, public userSrv: UsersService, public commonSrv: CommonService, public router: Router) { }

  ngOnInit(): void {
  }

  /* Call APIs */
  signOff() {
    Swal.fire({
      icon: "warning",
      title: `Confirmación`,
      text: `¿Seguro desea cerrar su sesión?`,
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonText: "SI",
      confirmButtonColor: "#3B82F6",
      cancelButtonText: "NO",
    }).then((result) => {
      if (result.isConfirmed) {
        localStorage.clear()
        this.router.navigateByUrl('sign-in')
      }
    })
  }
}
