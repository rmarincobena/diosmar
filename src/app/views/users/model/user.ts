import { IUser } from "../interface/iuser";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

export class User implements IUser {
    identificacion: string;
    nombres: string;
    apellidos: string;
    fechaNac: Date;
    genero: string;
    direccion: string;
    telefono: string;
    numeroHijos: number;
    activo?: boolean | undefined;
    isEdit: boolean = false

    constructor() {
        this.identificacion = ""
        this.nombres = ""
        this.apellidos = ""
        this.fechaNac = new Date()
        this.genero = ""
        this.direccion = ""
        this.telefono = ""
        this.numeroHijos = 0
        this.activo = true
        this.isEdit = false
    }

    newObject(payload: any): any {
        const entity = new User();
        entity.identificacion = payload["identificacion"]
        entity.nombres = payload["nombres"]
        entity.apellidos = payload["apellidos"]
        entity.fechaNac = payload["fechaNac"]
        entity.genero = payload["genero"]
        entity.direccion = payload["direccion"]
        entity.telefono = payload["telefono"]
        entity.numeroHijos = payload["numeroHijos"]
        entity.activo = payload["activo"]
        entity.isEdit = true
        return entity
    }

    formBuilder(fb: FormBuilder): FormGroup {
        return fb.group({
            identificacion: [this.identificacion, Validators.compose([Validators.required])!],
            nombres: [this.nombres, Validators.compose([Validators.required])!],
            apellidos: [this.apellidos, Validators.compose([Validators.required])!],
            fechaNac: [this.fechaNac, Validators.compose([Validators.required])!],
            genero: [this.genero, Validators.compose([Validators.required])!],
            direccion: [this.direccion, Validators.compose([Validators.nullValidator])!],
            telefono: [this.telefono, Validators.compose([Validators.nullValidator])!],
            numeroHijos: [this.numeroHijos, Validators.compose([Validators.required])!],
            activo: [this.activo, Validators.compose([Validators.required])!],
            isEdit: [this.isEdit]
        })
    }

}
