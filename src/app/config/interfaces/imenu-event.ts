export interface IMenuEvent {
    key: string;
    routeEvent?: boolean;
}
